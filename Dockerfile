FROM python:3.9

WORKDIR /app
COPY . /app
RUN pip install -r /app/requirements.txt

CMD ["gunicorn", "--log-level", "error", "-w", "4", "-k", "uvicorn.workers.UvicornWorker", "app.main:app", "-b", "0.0.0.0:8000"]
