from bson import ObjectId
from bson.errors import InvalidId
from typing import Optional, List, Any

from pydantic import BaseModel, BaseConfig, Field

from app.db.security import generate_salt, verify_password, get_password_hash


class OID(str):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        try:
            return ObjectId(str(v))
        except InvalidId:
            raise ValueError("Not a valid ObjectId")


class DBMixin(BaseModel):
    id: OID = Field(alias='_id')

    class Config(BaseConfig):
        allow_population_by_alias = True
        json_encoders = {
            ObjectId: lambda oid: str(oid),
        }


class ResponseModel(BaseModel):
    success: bool
    msg: Any

    class Config(BaseConfig):
        json_encoders = {ObjectId: str}


class MessageModel(BaseModel):
    detail: Any


class User(BaseModel):
    username: str
    display_name: Optional[str] = None
    disabled: Optional[bool] = False
    permissions: Optional[List[str]] = []


class UserInDB(DBMixin, User):
    salt: str = ""
    hashed_password: str = ""

    def check_password(self, password: str):
        return verify_password(self.salt + password, self.hashed_password)

    def change_password(self, password: str):
        self.salt = generate_salt()
        self.hashed_password = get_password_hash(self.salt + password)


class Image(BaseModel):
    name: str
    data: bytes


class ImageInDB(DBMixin, Image):
    pass
