import os
import motor.motor_asyncio

platform = os.getenv("PLATFORM")
mongo_url = os.getenv("MONGODB_URL")
print(mongo_url)

if platform == 'docker':
    MONGODB_URL = mongo_url
else:
    MONGODB_URL = "mongodb://127.0.0.1:27017"


client = motor.motor_asyncio.AsyncIOMotorClient(MONGODB_URL)

user_db = client.waller['users']
img_db = client.waller['images']
