from typing import Union, List, Any


async def get_update_fields(data: Union[List[Any], Any]) -> Union[dict, list]:
    if type(data) == list:
        return [{k: v for k, v in obj.dict().items() if v is not None} for obj in data]
    else:
        return {k: v for k, v in data.dict().items() if v is not None}
