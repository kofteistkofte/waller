import os
from mimetypes import guess_type

from fastapi import Depends, FastAPI, File, UploadFile, Response

from app.db.mongodb import img_db
from app.db.auth import get_user
from app.models import User, Image, ResponseModel


app = FastAPI(
    title='Waller',
    description="A small application for randomly selected wallpapers. \n"
                "It designed to be used against your fellow co-workers who forgot to lock their PCs.",
    version='0.1.2',
    debug=os.getenv("DEBUG", False)
)


@app.get('/users/me')
def read_current_user(user: User = Depends(get_user)):
    return {"username": user.username, "permissions": user.permissions}


@app.get('/', status_code=200)
async def get_random_image():
    random_img = img_db.aggregate([{'$sample': {'size': 1}}])
    async for i in random_img:
        return Response(
            content=i['data'],
            headers={
                'Content-Disposition': f"inline; filename=\"{i['name']}\""
            },
            media_type=guess_type(i['name'])[0],
            status_code=200,
        )


@app.post('/upload', status_code=201,
          response_model=ResponseModel)
async def upload_image(
    file: UploadFile = File(...),
    user: User = Depends(get_user)
):
    file_data = await file.read()

    image = Image(
        name=file.filename,
        data=file_data
    )

    image_in_db = await img_db.insert_one(image.dict())

    return {
        'success': image_in_db.acknowledged,
        'msg': image_in_db.inserted_id,
    }
